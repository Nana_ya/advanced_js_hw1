class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name(){
        return this._name;
    }
    set name(value){
        if (value === undefined | !isNaN(value)) {
            alert("Some symbols are unacceptable for names!");
            return;
        }
        this._name = value;
    }
    get age(){
        return this._age;
    }
    set age(value){
        if (value < 18 | isNaN(value)) {
            alert("Some symbols are unacceptable for age!");
            return;
        }
        this._age = value;
    }    
    
    get salary(){
        return this._salary;
    }

    set salary(value){
        if (value < 50 | isNaN(value)) {
            alert("Something is wrong. Please, try again!");
            return;
        }
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    get salary(){
        return this._salary;
    }
    set salary(value){
        super.salary = value;
        this._salary *= 3;
    }
    get lang(){
        return this._lang;
    }
    set lang(value){
        if (!Array.isArray(value)){
            return;
        }
        this._lang = value;
    }
}

let proger1 = new Programmer("John", 18, 100, ["english"]);
let proger2 = new Programmer("Nana", 19, 1000, ["english", "ukrainian"]);
let proger3 = new Programmer("Lola", 30, 200, ["english", "italian"]);
console.log(proger1);
console.log(proger2);
console.log(proger3);
